//
// Created by Paweł Ładna on 10/01/2020.
//

#ifndef PP_PROJEKT_MAIN_H
#define PP_PROJEKT_MAIN_H

void addNewElement();
void removeElement();
void getNumberOfElements();

#endif //PP_PROJEKT_MAIN_H
