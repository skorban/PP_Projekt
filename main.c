#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
 * PROJEKT: Implementacja listy.
 *
 * Proszę o przesłanie implementacji listy.
 * Lista musi umożliwić spis listy samochodów na parkingu. Implementacja powinna zawierać :
 *
 *
 * - addNewElement -> dodanie nowego samochodu wjeżdzającego na parking
 * - removeElement -> usuniecie samochodu wyjezdzajacego z parkingu
 * - getNumberOfElements -> zwraca liczbę elementów listy
 */

typedef struct Car //Definicja struktury List
    {
        char *RegNumb;
        char color;
        time_t entryTime;
        struct Car *next;
    }
    Car; //todo co robi ta cześć?
Car *pRoot = NULL;

void initList(void) //todo co to? Czy to inicjalizacja listy czy jej główny pojemnik?
{
    pRoot = malloc(sizeof(*pRoot));
    pRoot->RegNumb = malloc(strlen("Root") + 1u);
    strcpy(pRoot->RegNumb, "Root");
    time(&(pRoot->entryTime)); pRoot->next=NULL;
}

void deinitList(void) {
    free(pRoot->RegNumb);
    free(pRoot); }

struct Lista
{

};

void addNewElement() //todo Stworzyć funckję
{

}

void removeElement() //todo Stworzyć funckję
{

}

void getNumberOfElements() //todo Stworzyć funckję
{

}

int main() {
    int option = 0;

    printf("\nPodstawy programowania: Zadanie projektowe\n"); //Ładnie zaprojektowane menu dla konsoli
    printf("\nWybierz co chcesz zrobić:");
    printf("\nWybierz (1): Aby dodać nowy element.");
    printf("\nWybierz (2): Aby usunąć element.");
    printf("\nWybierz (3): Aby uzyskać liczbę elementów listy.\n");
    scanf("%d", &option);
    printf("Wybrano %d", option);

    switch (option)
    {
        case 1: //todo uruchomić funkcję addNewElement
            addNewElement();
            break;
        case 2: //todo uruchomić funckję removeElement
            removeElement();
            break;
        case 3:
            getNumberOfElements();
            break;
        default: //todo uruchomić funkcję getNumberOfElements
            printf("\nBłąd wyboru. Domyślnie wyświetlam liczbę elementów funkcji");
            getNumberOfElements();
            break;
    }
    puts("\nKoniec programu");
    return 0;
}
